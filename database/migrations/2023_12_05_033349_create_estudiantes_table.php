<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('estudiantes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('clase_id')->constrained(
                table: 'clases', indexName: 'estudiantes_clase_id'
            );
            $table->foreignId('seccion_id')->constrained(
                table: 'secciones', indexName: 'estudiantes_seccion_id'
            );
            $table->string('nombre');
            $table->string('correo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('estudiantes');
    }
};
