<?php

namespace Database\Seeders;

use App\Models\Clases;
use App\Models\Estudiante;
use App\Models\Secciones;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class ClasesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Clases::factory()
            ->count(5)
            ->sequence(fn ($sequence) => ['nombre' => 'Clase ' . $sequence->index + 1])
            ->has(
                Secciones::factory()
                    ->count(2)
                    ->state(
                        new Sequence(
                            ['nombre' => 'Sección A'],
                            ['nombre' => 'Sección B'],
                        )
                    )
                    ->has(
                        Estudiante::factory()
                            ->count(2)
                            ->state(
                                function (array $attributes, Secciones $section) {
                                    return ['clase_id' => $section->clase_id];
                                }
                            )
                    )
            )
            ->create();
    }
}
