<?php

use App\Http\Controllers\EstudianteController;
use App\Http\Controllers\ProfileController;
use App\Livewire\Estudiante\Edit;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    // Ruta de recursos para el estudiante
    // Llama al controlador Estudiante de Laravel
    
    Route::resource('estudiantes', EstudianteController::class);

    // Ruta tipo get para actualizar un estudiante por medio de su id
    // Llama al componente Edit de livewire
    Route::get('estudiantes/{estudiante}/edit', Edit::class)->name('estudiantes.edit');
});

require __DIR__.'/auth.php';
