<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Estudiante extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;
    protected $fillable = [
        'seccion_id',
        'clase_id',
        'nombre',
        'correo'
    ];

    public function secciones(){
        return $this->belongsTo(Secciones::class, 'seccion_id');
    }

    public function clases(){
        return $this->belongsTo(Clases::class, 'clase_id');
    }
}
