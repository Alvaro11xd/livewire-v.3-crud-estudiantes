<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Secciones extends Model
{
    use HasFactory;
    protected $fillable = [
        'clase_id',
        'nombre'
    ];
    public function clases(){
        return $this->belongsTo(Clases::class, 'clase_id');
    }

    public function estudiantes(){
        return $this->hasMany(Estudiante::class, 'seccion_id');
    }
}
