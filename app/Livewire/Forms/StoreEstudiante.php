<?php

namespace App\Livewire\Forms;

use App\Models\Estudiante;
use Livewire\Attributes\Validate;
use Livewire\Form;

class StoreEstudiante extends Form
{
    #[Validate('required|min:3')]
    public $nombre;

    #[Validate('required|email')]
    public $correo;

    #[Validate('required|image|max:1024')]
    public $imagen;

    #[Validate('required')]
    public $seccion_id;

    public function guardar($clase_id)
    {
        $estudiante = Estudiante::create(

            $this->all() + ['clase_id' => $clase_id],
        );

        $estudiante
            ->addMedia($this->imagen)
            ->toMediaCollection();
    }
}
