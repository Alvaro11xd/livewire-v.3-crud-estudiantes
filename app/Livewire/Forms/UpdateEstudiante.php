<?php

namespace App\Livewire\Forms;

use App\Models\Estudiante;
use Livewire\Attributes\Validate;
use Livewire\Form;

class UpdateEstudiante extends Form
{
    public Estudiante $estudiante;

    #[Validate('required|min:3')]
    public $nombre;

    #[Validate('required|email')]
    public $correo;

    #[Validate('nullable|image|max:1024')]
    public $imagen;

    #[Validate('required')]
    public $seccion_id;

    public function setStudent(Estudiante $estudiante)
    {
        $this->estudiante = $estudiante;

        $this->nombre = $estudiante->nombre;
        $this->seccion_id = $estudiante->seccion_id;
        $this->correo = $estudiante->correo;
    }

    public function actualizar($clase_id)
    {
        $this->estudiante->update([
            'nombre' => $this->nombre,
            'clase_id' => $clase_id,
            'seccion_id' => $this->seccion_id,
            'correo' => $this->correo
        ]);

        if ($this->imagen) {
            $this->estudiante
                ->addMedia($this->imagen)
                ->toMediaCollection();
        }
    }
}
