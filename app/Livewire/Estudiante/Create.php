<?php

namespace App\Livewire\Estudiante;

use App\Livewire\Forms\StoreEstudiante;
use App\Models\Clases;
use App\Models\Secciones;
use Livewire\Attributes\Validate;
use Livewire\Component;
use Livewire\WithFileUploads;

class Create extends Component
{
    use WithFileUploads;

    public StoreEstudiante $form;

    #[Validate('required')]
    public $clase_id;

    public $secciones = [];

    public function render()
    {
        return view('livewire.estudiante.create', [
            'clases' => Clases::all()
        ]);
    }

    public function guardar()
    {
        $this->validate();

        $this->form->guardar(clase_id: $this->clase_id);

        return redirect()->route('estudiantes.index')
            ->with('estatus', 'Estudiante creado con éxito');
    }

    public function updatedClaseId($value)
    {
        $this->secciones = Secciones::where('clase_id', $value)->get();
    }
}
