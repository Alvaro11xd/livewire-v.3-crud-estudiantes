<?php

namespace App\Livewire\Estudiante;

use App\Livewire\Forms\UpdateEstudiante;
use App\Models\Clases;
use App\Models\Estudiante;
use App\Models\Secciones;
use Livewire\Attributes\Layout;
use Livewire\Attributes\Validate;
use Livewire\WithFileUploads;
use Livewire\Component;

class Edit extends Component
{
    use WithFileUploads;

    public Estudiante $estudiante;

    public UpdateEstudiante $form;

    #[Validate('required')]
    public $clase_id;

    public $secciones = [];

    public function mount()
    {

        $this->form->setStudent($this->estudiante);

        $this->fill(
            $this->estudiante->only('clase_id')
        );

        $this->secciones = Secciones::where('clase_id', $this->estudiante->clase_id)->get();
    }

    #[Layout('layouts.app')]
    public function render()
    {
        return view('livewire.estudiante.edit', [
            'clases' => Clases::all()
        ]);
    }

    public function updatedClaseId($value)
    {
        $this->secciones = Secciones::where('clase_id', $value)->get();
    }

    public function actualizar()
    {
        $this->validate();

        $this->form->actualizar($this->clase_id);

        return redirect()->route('estudiantes.index')
            ->with('estatus', 'El estudiante ha sido actualizado con éxito');
    }
}
