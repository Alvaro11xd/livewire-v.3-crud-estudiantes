<?php

namespace App\Livewire\Estudiante;

use App\Models\Estudiante;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;
    public function render()
    {
        return view('livewire.estudiante.index', [
            'estudiantes' => Estudiante::paginate(10),
        ]);
    }

    public function eliminar(Estudiante $estudiante)
    {
        $estudiante->delete();

        return redirect()->route('estudiantes.index')
            ->with('estatus', 'El estudiante ha sido eliminado con éxito');
    }

    
}
