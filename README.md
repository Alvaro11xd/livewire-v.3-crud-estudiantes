<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

## CRUD DE ESTUDIANTES CON LIVEWIRE

Tecnologías usadas en este proyecto

- Livewire v.3 [https://livewire.laravel.com/docs/installation]
- Laravel Breeze [https://laravel.com/docs/10.x/starter-kits#laravel-breeze]
- Vite Livewire Plugin [https://github.com/defstudio/vite-livewire-plugin]
- Laravel - Medialibrary -> para subir archivos [https://spatie.be/docs/laravel-medialibrary/v10/introduction]
