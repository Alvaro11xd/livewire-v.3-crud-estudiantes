<div class="max-w-7xl mx-auto py-6 sm:px-6 lg:px-8">
    <div class="lg:grid lg:grid-cols-12 lg:gap-x-5">
        <div class="space-y-6 sm:px-6 lg:px-0 lg:col-span-12">

            {{-- Formulario para actualizar con livewire --}}
            {{-- Evento submit para actualizar --}}

            <form wire:submit="actualizar">
                <div class="shadow sm:rounded-md sm:overflow-hidden">
                    <div class="bg-white py-6 px-4 space-y-6 sm:p-6">
                        <div>
                            <h3 class="text-lg leading-6 font-medium text-gray-900">
                                Información del Estudiante
                            </h3>
                            <p class="mt-1 text-sm text-gray-500">
                                Edite los campos necesarios para actualizar la información del estudiante
                            </p>
                        </div>

                        <div class="grid grid-cols-6 gap-6">
                            <div class="col-span-6 sm:col-span-3">
                                <label for="nombre" class="block text-sm font-medium text-gray-700">Nombre</label>

                                {{-- Propiedad NOMBRE que viene de un Form que ofrece Livewire --}}
                                <input wire:model="form.nombre" type="text" id="nombre"
                                    class="mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" />
                                <div class="text-red-500">
                                    @error('nombre')
                                        {{ $message }}
                                    @enderror
                                </div>

                            </div>

                            <div class="col-span-6 sm:col-span-3">
                                <label for="correo" class="block text-sm font-medium text-gray-700">Correo
                                    Electrónico</label>

                                    {{-- Propiedad CORREO que viene de un Form que ofrece Livewire --}}

                                <input wire:model="form.correo" type="text" id="correo" autocomplete="correo"
                                    class="mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" />
                                <div class="text-red-500">
                                    @error('correo')
                                        {{ $message }}
                                    @enderror
                                </div>
                            </div>

                            {{-- Verificando que la imagen exista y mostrando la imagen --}}
                            @if (
                                $estudiante
                                    ?->getMedia()
                                    ?->last()
                                    ?->getUrl())
                                <div class="col-span-6 sm:col-span-4">
                                    <img src="{{ $estudiante?->getMedia()?->last()?->getUrl() }}"
                                        alt="{{ $estudiante->nombre }}" width="200px" />
                                </div>
                            @endif

                            <div class="col-span-6 sm:col-span-4">
                                <label for="imagen" class="block text-sm font-medium text-gray-700">Imagen</label>

                                {{-- Input FILE para subir una imagen --}}
                                <input wire:model="form.imagen" type="file" id="imagen"
                                    class="mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" />
                                <div class="text-red-500">
                                    @error('imagen')
                                        {{ $message }}
                                    @enderror
                                </div>
                            </div>

                            <div class="col-span-6 sm:col-span-3">
                                <label for="clase_id" class="block text-sm font-medium text-gray-700">Clase</label>

                                {{-- Select para elegir una clase --}}

                                <select wire:model.live="clase_id" id="clase_id"
                                    class="mt-1 block w-full bg-white border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                                    <option value="">Selecciona una Clase</option>
                                    @foreach ($clases as $clase)
                                        <option value="{{ $clase->id }}">
                                            {{ $clase->nombre }}
                                        </option>
                                    @endforeach
                                </select>
                                <div class="text-red-500">
                                    @error('clase_id')
                                        {{ $message }}
                                    @enderror
                                </div>
                            </div>

                            <div class="col-span-6 sm:col-span-3">
                                <label for="seccion_id" class="block text-sm font-medium text-gray-700">Sección</label>
                                <select wire:model="form.seccion_id" id="seccion_id"
                                    class="mt-1 block w-full bg-white border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                                    <option value="">Selecciona una sección</option>
                                    @foreach ($secciones as $seccion)
                                        <option value="{{ $seccion->id }}">
                                            {{ $seccion->nombre }} - {{ $seccion->clases->nombre }}
                                        </option>
                                    @endforeach
                                </select>
                                <div class="text-red-500">
                                    @error('seccion_id')
                                        {{ $message }}
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">

                        {{-- Botón para cancelar --}}

                        <a href="{{ route('estudiantes.index') }}" as="button"
                            class="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md text-indigo-700 bg-indigo-100 hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            Cancelar
                        </a>

                        {{-- Botón para actualizar --}}

                        <button type="submit"
                            class="bg-indigo-600 border border-transparent rounded-md shadow-sm py-2 px-4 inline-flex justify-center text-sm font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            Actualizar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
