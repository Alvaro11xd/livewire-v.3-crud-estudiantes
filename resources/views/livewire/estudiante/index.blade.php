<div class="mt-8 flex flex-col">

    {{-- Componente para mostrar los mensajes de estado --}}
    <x-auth-session-status class="mb-4" :status="session('estatus')" />

    <div class="-my-2 -mx-4 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
            <div class="overflow-hidden shadow ring-1 ring-black ring-opacity-5 md:rounded-lg">
                <table class="min-w-full divide-y divide-gray-300">
                    <thead class="bg-gray-50">
                        <tr>
                            <th scope="col"
                                class="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-6">
                                ID
                            </th>
                            <th scope="col"
                                class="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-6">
                                Nombre
                            </th>
                            <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                Correo electrónico
                            </th>
                            <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                Imagen
                            </th>
                            <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                Clase
                            </th>
                            <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                Sección
                            </th>
                            <th scope="col" class="relative py-3.5 pl-3 pr-4 sm:pr-6" />
                        </tr>
                    </thead>
                    <tbody class="divide-y divide-gray-200 bg-white">
                        @foreach ($estudiantes as $estudiante)
                            <tr>
                                <td class="whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-6">
                                    {{ $estudiante->id }}
                                </td>
                                <td class="whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-6">
                                    {{ $estudiante->nombre }}
                                </td>
                                <td class="whitespace-nowrap px-3 py-4 text-sm text-gray-500">
                                    {{ $estudiante->correo }}
                                </td>
                                <td class="whitespace-nowrap px-3 py-4 text-sm text-gray-500">
                                    Mostrando la imagen con laravel - medialibrary
                                    <img src="{{ $estudiante?->getMedia()?->last()?->getUrl() }}" alt=""
                                        width="200px" />
                                </td>
                                <td class="whitespace-nowrap px-3 py-4 text-sm text-gray-500">
                                    {{ $estudiante->clases->nombre }}
                                </td>
                                <td class="whitespace-nowrap px-3 py-4 text-sm text-gray-500">
                                    {{ $estudiante->secciones->nombre }}
                                </td>

                                <td
                                    class="relative whitespace-nowrap py-4 pl-3 pr-4 text-right text-sm font-medium sm:pr-6">

                                    {{-- Botón que lleva a la vista de editar --}}
                                    <a wire:navigate href="{{ route('estudiantes.edit', $estudiante->id) }}"
                                        class="text-indigo-600 hover:text-indigo-900">
                                        Editar
                                    </a>

                                    {{-- Botón que elimina un registro --}}
                                    {{-- Evento click de livewire para eliminar un registro --}}
                                    <button wire:confirm="¿Estás seguro de eliminar el registro?"
                                        wire:click="eliminar({{ $estudiante->id }})"
                                        class="ml-2 text-indigo-600 hover:text-indigo-900">
                                        Eliminar
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            {{-- Paginación con livewire --}}
            <div class="mt-5">
                {{ $estudiantes->links() }}
            </div>
        </div>
    </div>
</div>

